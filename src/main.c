#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/eriks_freemem/freemem.h"
#include <avr/pgmspace.h>
#include "uart.h"
#include "hmi.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include <string.h>

#define BLINK_DELAY_MS 100

static inline void init_leds(void)
{
    /* Set port A pins 23, 25, 27 for output*/
    DDRA |= _BV(PA1);
    DDRA |= _BV(PA3);
    DDRA |= _BV(PA5);
    /*Turn built-in yellow LED off*/
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}

static inline void init_lcd(void)
{
    lcd_init();
    lcd_goto(LCD_ROW_1_START);
    lcd_puts_P(stud_name);
}

static inline void init_console(void)
{
    fprintf_P(stdout, PSTR("\nConsole Started \n"));
    fprintf_P(stdout, stud_name);
    fprintf(stdout, "\n");
    print_banner_P(stdout, banner, BANNER_ROWS);
}

static inline void init_errcon(void)
{
    simple_uart1_init();
    stderr = &simple_uart1_out;
    fprintf(stderr, "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__ "\n");
    fprintf(stderr, "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " "
            "avr-gcc version: " __VERSION__ "\n");
}

static inline void month_lookup_P(void)
{
    char in_buf = 0;
    printf_P(PSTR("Enter month name's first letter :"));
    scanf("%c", &in_buf);
    printf("%c\n", in_buf);
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    for (uint8_t i = 0; i < NAME_MONTH_COUNT; i++) {
        if (!strncmp_P(&in_buf, (PGM_P) pgm_read_word(&(name_month[i])), 1)) {
            fprintf_P(stdout, (PGM_P) pgm_read_word(&(name_month[i])));
            fputc('\n', stdout);
            lcd_puts_P((PGM_P) pgm_read_word(&(name_month[i])));
            lcd_putc(' ');
        }
    }
}

static inline void blink_leds(void)
{
    /* Set port A pin 1 high to turn red LED on*/
    PORTA |= _BV(PORTA1);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 1 low to turn red LED off*/
    PORTA &= ~_BV(PORTA1);
    /* Set port A pin 3 high to turn green LED on*/
    PORTA |= _BV(PORTA3);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 3 low to turn green LED off*/
    PORTA &= ~_BV(PORTA3);
    /* Set port A pin 5 high to turn blue LED on*/
    PORTA |= _BV(PORTA5);
    _delay_ms(BLINK_DELAY_MS);
    /* Set port A pin 5 low to turn blue LED off*/
    PORTA &= ~_BV(PORTA5);
}


void main(void)
{
    init_leds();
    init_errcon();
    init_lcd();
    init_console();

    while (1) {
        blink_leds();
        month_lookup_P();
    }
}