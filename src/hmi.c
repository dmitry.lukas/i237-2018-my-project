#include "hmi.h"
#include <avr/pgmspace.h>

const char stud_name[] PROGMEM = "Dmitri Lukas";

const char m1[] PROGMEM = "January";
const char m2[] PROGMEM = "February";
const char m3[] PROGMEM = "March";
const char m4[] PROGMEM = "April";
const char m5[] PROGMEM = "May";
const char m6[] PROGMEM = "June";

PGM_P const name_month[] PROGMEM = {
    m1,
    m2,
    m3,
    m4,
    m5,
    m6
};

const char string_1[] PROGMEM = "            (                 ,&&&.";
const char string_2[] PROGMEM = "             )                .,.&&";
const char string_3[] PROGMEM = "            (  (              \\=__/";
const char string_4[] PROGMEM = "          (    (  ,,      _.__|/ /|";
const char string_5[] PROGMEM = "           ) /\\ -((------((_|___/ |";
const char string_6[] PROGMEM = "         (  // | (`'      ((  `'--|";
const char string_7[] PROGMEM = "       _ -.;_/ \\--._      \\ \\-._/.";
const char string_8[] PROGMEM = "      (_;-// | \\ \\-'.\\    <_,\\_\\`--'|";
const char string_9[] PROGMEM = "      ( `.__ _  ___,')      <_,-'__,'";
const char string_10[] PROGMEM = "      `'(_ )_)(_)_)'";

PGM_P const banner[] PROGMEM = {
    string_1,
    string_2,
    string_3,
    string_4,
    string_5,
    string_6,
    string_7,
    string_8,
    string_9,
    string_10
};

